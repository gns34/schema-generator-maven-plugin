package ru.gns34.schema.generator.maven.plugin.generate.json;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import org.apache.maven.plugin.logging.Log;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.module.jsonSchema.JsonSchema;
import com.fasterxml.jackson.module.jsonSchema.customProperties.HyperSchemaFactoryWrapper;
import com.fasterxml.jackson.module.jsonSchema.factories.SchemaFactoryWrapper;

import ru.gns34.schema.generator.maven.plugin.PluginConfig;

public class GeneratorJson {

	private static final ObjectMapper MAPPER = new ObjectMapper();
	
	private final SchemaFactoryWrapper schemaVisitor;

	private final Log log;
	private final File outputDirectory; 
	private final Class<?> classRq;
	private final Class<?> classRs;
	
	public GeneratorJson(PluginConfig config) {
		this.schemaVisitor = new HyperSchemaFactoryWrapper();
		this.schemaVisitor.setVisitorContext(new LinkVisitorContext());
		
		this.log = config.getLog();
		this.outputDirectory = config.getOutputDirectory(); 
		this.classRq = config.getClassRq();
		this.classRs = config.getClassRs();
	}
	
	public void generate() throws GeneratorJsonException {
		generateFile(classRq);
		generateFile(classRs);
	}

	private void generateFile(Class<?> clazz) throws GeneratorJsonException {
		try {
			MAPPER.acceptJsonFormatVisitor(MAPPER.constructType(clazz), this.schemaVisitor);
		} catch (Exception e) {
			throw new GeneratorJsonException("������ ������������ ������ " + clazz, e);
		}
		final JsonSchema schema = this.schemaVisitor.finalSchema();
		String json = null;
		try {
			json = MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(schema);
		} catch (Exception e) {
			throw new GeneratorJsonException("������ ����������� ����� ��� ������ " + clazz, e);
		}
		try {
			Path  outputPath = Paths.get(this.outputDirectory + File.separator + clazz.getSimpleName() + ".json");
			Files.write(outputPath, json.getBytes(), StandardOpenOption.WRITE, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
			log.info("Created JSON Schema: " + outputPath.normalize().toAbsolutePath().toString());
		} catch (Exception e) {
			throw new GeneratorJsonException("������ ������ ����� ��� ����� ������ " + clazz, e);
		}
	}
}
