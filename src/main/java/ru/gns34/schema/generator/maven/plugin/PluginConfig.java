package ru.gns34.schema.generator.maven.plugin;

import java.io.File;

import org.apache.maven.model.Model;
import org.apache.maven.plugin.logging.Log;

public class PluginConfig {

	private Model projectInfo;
	private Log log;
	private File outputDirectory; 
	private Class<?> classRq; 
	private Class<?> classRs;
	
	public Model getProjectInfo() {
		return projectInfo;
	}
	void setProjectInfo(Model projectInfo) {
		this.projectInfo = projectInfo;
	}
	public Log getLog() {
		return log;
	}
	void setLog(Log log) {
		this.log = log;
	}
	public File getOutputDirectory() {
		return outputDirectory;
	}
	void setOutputDirectory(File outputDirectory) {
		this.outputDirectory = outputDirectory;
	}
	public Class<?> getClassRq() {
		return classRq;
	}
	void setClassRq(Class<?> classRq) {
		this.classRq = classRq;
	}
	public Class<?> getClassRs() {
		return classRs;
	}
	void setClassRs(Class<?> classRs) {
		this.classRs = classRs;
	}
	
}
