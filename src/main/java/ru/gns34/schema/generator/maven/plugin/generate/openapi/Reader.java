package ru.gns34.schema.generator.maven.plugin.generate.openapi;

import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import io.swagger.v3.core.converter.AnnotatedType;
import io.swagger.v3.core.converter.ModelConverters;
import io.swagger.v3.core.converter.ResolvedSchema;
import io.swagger.v3.core.util.PrimitiveType;
import io.swagger.v3.oas.integration.api.OpenAPIConfiguration;
import io.swagger.v3.oas.integration.api.OpenApiReader;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.oas.models.PathItem;
import io.swagger.v3.oas.models.Paths;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.media.Content;
import io.swagger.v3.oas.models.media.MediaType;
import io.swagger.v3.oas.models.media.Schema;
import io.swagger.v3.oas.models.parameters.RequestBody;
import io.swagger.v3.oas.models.responses.ApiResponse;
import io.swagger.v3.oas.models.responses.ApiResponses;

public class Reader implements OpenApiReader {

	public static final String INFO = "info";
	public static final String RESPONSE = "response";
	public static final String REQUEST = "request";

	private static final String COMPONENTS_REF = "#/components/schemas/";
	
	private final OpenAPI openAPI = new OpenAPI();
	private final Components components = new Components();
	
	public Reader() {
		openAPI.setComponents(components);
	}
	
	@Override
	public void setConfiguration(OpenAPIConfiguration openApiConfiguration) {
	}

	@Override
	public OpenAPI read(Set<Class<?>> classes, Map<String, Object> resources) {
		openAPI.setInfo(mergeInfo((Info) resources.get(INFO)));
		openAPI.setExternalDocs(generateDocs(resources));
		openAPI.setPaths(getPaths(resources));
		return openAPI;
	}

	private Paths getPaths(Map<String, Object> resources) {
		Paths paths = new Paths();
		PathItem item = new PathItem();
		item.post(getOperation(resources));
		paths.put("/doOperation", item);
		return paths;
	}

	private ExternalDocumentation generateDocs(Map<String, Object> resources) {
		if (resources.get("dosc") == null) {
			return null;
		}
		ExternalDocumentation doc = new ExternalDocumentation();
		return doc;
	}

	private Info mergeInfo(Info in) {
		final Info mergeInfo;
		if (in != null) {
			mergeInfo = in;
		} else {
			mergeInfo = new Info();
		}
		if (StringUtils.isBlank(mergeInfo.getTitle())) {
			mergeInfo.setTitle("���");
		}
		if (StringUtils.isBlank(mergeInfo.getDescription())) {
			mergeInfo.setDescription("��������� ����������� ��������������");
		}
		if (StringUtils.isBlank(mergeInfo.getVersion())) {
			mergeInfo.setVersion("1.0");
		}
		return mergeInfo;
	}

	private Operation getOperation(Map<String, Object> resources) {
		Operation operation = new Operation();
		// RequestBody in Method
		operation.setSummary("��������� ��������");
		operation.setRequestBody(getRequestBody(components, (Class<?>) resources.get(REQUEST)));
		operation.setOperationId("doOperation"); // �������� ������ ���
		// classResponses
		operation.setResponses(getApiResponses(components, (Class<?>) resources.get(RESPONSE)));
		return operation;
	}
	
	private static RequestBody getRequestBody(Components components, Class<?> schemaImplementation) {
        RequestBody requestBodyObject = new RequestBody();
        requestBodyObject.setDescription("������");
        requestBodyObject.setRequired(true);
        requestBodyObject.setContent(getContent(components, schemaImplementation));
        return requestBodyObject;
    }

	private static ApiResponses getApiResponses(Components components, Class<?> schemaImplementation) {
        ApiResponses apiResponsesObject = new ApiResponses();
        ApiResponse apiResponseObject = new ApiResponse();
        apiResponsesObject.addApiResponse("200", apiResponseObject);
        apiResponseObject.setDescription("��������� ���������� ��������");
        apiResponseObject.content(getContent(components, schemaImplementation));
        apiResponsesObject._default(apiResponseObject);
        return apiResponsesObject;
    }
    
	private static Content getContent(Components components, Class<?> schemaImplementation) {
    	//Encapsulating Content model
        Content content = new Content();
        MediaType mediaType = new MediaType();
        mediaType.setSchema(resolveSchemaFromType(schemaImplementation, components));
        content.addMediaType("application/json", mediaType);
        return content;
    }
    
    @SuppressWarnings("rawtypes")
    private static Schema resolveSchemaFromType(Class<?> schemaImplementation, Components components) {
        Schema schemaObject;
        PrimitiveType primitiveType = PrimitiveType.fromType(schemaImplementation);
        if (primitiveType != null) {
            schemaObject = primitiveType.createProperty();
        } else {
            schemaObject = new Schema();
            ResolvedSchema resolvedSchema = ModelConverters.getInstance().readAllAsResolvedSchema(new AnnotatedType().type(schemaImplementation));
            Map<String, Schema> schemaMap;
            if (resolvedSchema != null) {
                schemaMap = resolvedSchema.referencedSchemas;
                schemaMap.forEach((key, referencedSchema) -> {
                    if (components != null) {
                        components.addSchemas(key, referencedSchema);
                    }
                });
                if (StringUtils.isNotBlank(resolvedSchema.schema.getName())) {
                    schemaObject.set$ref(COMPONENTS_REF + resolvedSchema.schema.getName());
                } else {
                    schemaObject = resolvedSchema.schema;
                }
            }
        }
        if (StringUtils.isBlank(schemaObject.get$ref()) && StringUtils.isBlank(schemaObject.getType())) {
            // default to string
            schemaObject.setType("string");
        }
        return schemaObject;
    }
}
