package ru.gns34.schema.generator.maven.plugin.generate.json;

public class GeneratorJsonException extends Exception {

	private static final long serialVersionUID = 1L;

	public GeneratorJsonException(String msg, Exception e) {
		super(msg, e);
	}

}
