package ru.gns34.schema.generator.maven.plugin;

import java.io.File;

/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;

import ru.gns34.schema.generator.maven.plugin.generate.json.GeneratorJson;
import ru.gns34.schema.generator.maven.plugin.generate.openapi.GeneratorOpenApi;

/**
 * Goal which touches a timestamp file.
 */
@Mojo(name = "schema-generator", 
requiresDependencyResolution = ResolutionScope.COMPILE,
configurator = "include-project-dependencies",
defaultPhase = LifecyclePhase.COMPILE)
public class SchemaGeneratorMojo extends AbstractMojo {
	
	@Parameter(defaultValue = "${project}", required = true, readonly = true)
	private MavenProject project;
	
	// ...\target\classes\
	@Parameter(property = "buildDirectory", required = true, defaultValue = "${project.build.outputDirectory}")
	private File outputDirectory;
	
	@Parameter(property = "classRequest", required = true)
	private String classRequest;
	@Parameter(property = "classResponse", required = true)
	private String classResponse;
	
	@Parameter(property = "json", required = true, defaultValue = "true")
	private boolean generateJson;
	@Parameter(property = "openapi", required = true, defaultValue = "true")
	private boolean generateOpenApi;
	@Parameter(property = "xsd", required = true, defaultValue = "true")
	private boolean generateXsd;

	public void execute() throws MojoExecutionException {
		final Class<?> classRq;
		try {
			classRq = getClass().getClassLoader().loadClass(this.classRequest);
		} catch (Exception e) {
			throw new MojoExecutionException("Error load " + classRequest, e);
		} 
		final Class<?> classRs;
		try {
			classRs = getClass().getClassLoader().loadClass(this.classResponse);
		} catch (Exception e) {
			throw new MojoExecutionException("Error load " + classResponse, e);
		}
		final PluginConfig config = collectedConfig(classRq, classRs);
		if (this.generateJson) {
			try {
				(new GeneratorJson(config)).generate();
			} catch (Exception e) {
				throw new MojoExecutionException("Error creating schema json", e);
			}
		}
		if (this.generateOpenApi) {
			try {
				(new GeneratorOpenApi(config)).generate();
			} catch (Exception e) {
				throw new MojoExecutionException("Error creating schema json", e);
			}
		}
	}

	private PluginConfig collectedConfig(Class<?> classRq, Class<?> classRs) {
		final PluginConfig config = new PluginConfig();
		config.setLog(getLog());
		config.setClassRq(classRq);
		config.setClassRs(classRs);
		config.setOutputDirectory(this.outputDirectory);
		config.setProjectInfo(this.project.getModel());
		return config;
	}
}
