package ru.gns34.schema.generator.maven.plugin.generate.openapi;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.apache.maven.model.Model;
import org.apache.maven.plugin.logging.Log;

import io.swagger.v3.core.util.Yaml;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import ru.gns34.schema.generator.maven.plugin.PluginConfig;
import ru.gns34.schema.generator.maven.plugin.generate.json.GeneratorJsonException;

public class GeneratorOpenApi {

	private static final boolean IS_PRETTY_PRINT = true;

	private final Log log;
	private final File outputDirectory; 
	private final Class<?> classRq;
	private final Class<?> classRs;
	private final Model projectInfo;
	
	public GeneratorOpenApi(PluginConfig config) {
		this.log = config.getLog();
		this.outputDirectory = config.getOutputDirectory(); 
		this.classRq = config.getClassRq();
		this.classRs = config.getClassRs();
		this.projectInfo = config.getProjectInfo();
	}
	
	public void generate() throws GeneratorJsonException {
		final Map<String, Object> resources = new HashMap<String, Object>();
		resources.put(Reader.REQUEST, this.classRq);
		resources.put(Reader.RESPONSE, this.classRs);
		resources.put(Reader.INFO, generateShemaInfo());
		
		final OpenAPI oas = (new Reader()).read(Collections.emptySet(), resources);
		
		String openApiYaml = null;
        try {
			openApiYaml =  IS_PRETTY_PRINT ? Yaml.pretty().writeValueAsString(oas) : Yaml.mapper().writeValueAsString(oas);
		} catch (Exception e) {
			throw new GeneratorJsonException("������ ����������� �����", e);
		}
        
        try {
			Path  outputPath = Paths.get(this.outputDirectory + File.separator + generateShemaName() + ".yaml");
			Files.write(outputPath, openApiYaml.getBytes(), StandardOpenOption.WRITE, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
			log.info("Created JSON Schema: " + outputPath.normalize().toAbsolutePath().toString());
		} catch (Exception e) {
			throw new GeneratorJsonException("������ ������ ����� ��� ����� ������ " + this.classRq, e);
		}
	}

	private Info generateShemaInfo() {
		final Info info = new Info();
		info.setTitle(this.projectInfo.getArtifactId());
		info.setDescription(this.projectInfo.getDescription());
		info.setVersion(this.projectInfo.getVersion());
		return info;
	}

	private String generateShemaName() {
		return this.projectInfo.getArtifactId();
	}
}
