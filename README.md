# schema-generator-maven-plugin
Плагин для генерации схем xsd,json и openApi v3 для взаиодействия АС - АС по схеме запрос/ответ


## Как подключить
<plugin>
	<groupId>ru.gns34</groupId>
	<artifactId>schema-generator-maven-plugin</artifactId>
	<version>0.1-SNAPSHOT</version>
	<configuration>
		<classRequest>ru.api.Request</classRequest>
		<classResponse>ru.api.Response</classResponse>
	</configuration>
	<executions>
		<execution>
			<goals>
				<goal>schema-generator</goal>
			</goals>
		</execution>
	</executions>
</plugin>

